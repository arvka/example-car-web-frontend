import IconCheck from './check-icon.png';
import Icon24 from './icon-24hrs.png';
import IconComplete from './icon-complete.png';
import IconPrice from './icon-price.png';
import IconProfessional from './icon-professional.png';

export {IconCheck, Icon24, IconComplete, IconPrice, IconProfessional}; 