import React from 'react'
import { Icon24, IconComplete, IconPrice, IconProfessional, ILWoman } from '../../../Assets';
import { colors } from '../../../Utils';
import CarouselCard from '../../Elements/CarouselCard';
import CheckItem from '../../Elements/CheckItem';
import InfoCard from '../../Elements/InfoCard';

export default function InfoFragment() {
    const styles = {
        container01 : {
            backgroundColor: colors.bgMainPage01,
            height: 'auto',
            overflowX: 'hidden',
            paddingTop: 97
        },
        container02 : {
            backgroundColor: colors.bgMainPage02,
            height: 'auto',
            overflowX: 'hidden'
        },
    };
    return (
        <div style={styles.container02}>
            <div className='row'>
                <div className='col-md-6' style={{display: 'flex', padding: 54, alignItems: 'center', justifyContent: 'end'}}>
                    <img src={ILWoman} style={{width: '100%', maxWidth: 459, maxHeight: 428}} alt='Woman service' />
                </div>
                <div className='col-md-6' style={{display: 'flex', alignItems: 'center', justifyContent: 'start'}}>
                    <div style={{margin: 16, maxWidth: 468}}>
                        <div style={{fontFamily: 'arial', fontSize: 24, fontWeight: 700, fontStyle: 'normal'}}>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</div>
                        <div style={{fontFamily: 'arial', fontSize: 14, fontWeight: 700, fontStyle: 'normal', marginTop: 24}} >Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</div>
                        <CheckItem text={'Sewa Mobil Dengan Supir di Bali 12 Jam'} />
                        <CheckItem text={'Sewa Mobil Lepas Kunci di Bali 24 Jam'} />
                        <CheckItem text={'Sewa Mobil Jangka Panjang Bulanan'} />
                        <CheckItem text={'Gratis Antar - Jemput Mobil di Bandara'} />
                        <CheckItem text={'Layanan Airport Transfer / Drop In Out'} />
                    </div>
                </div>
            </div>
            <div className='row'>
                <div className='col-12' style={{display: 'flex', alignItems: 'start', justifyContent: 'center'}}>
                    <div style={{display: 'flex', alignItems: 'start', justifyContent: 'center', flexDirection:'column'}}>
                        <div style={{paddingRight: 16, paddingLeft: 16}}>
                            <div style={{fontFamily: 'arial', fontSize: 24, fontWeight: 700, fontStyle: 'normal'}}>Why Us ?</div>
                            <div style={{fontFamily: 'arial', fontSize: 14, fontWeight: 700, fontStyle: 'normal', marginTop: 16}}>Mengapa harus pilih Binar Car Rental ?</div>
                        </div>
                        <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', marginTop: 8, marginBottom: 40}}>
                            <InfoCard icon={IconComplete} title={'Mobil Lengkap'} bodyText={'Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat'}/>
                            <InfoCard icon={IconPrice} title={'Harga Murah'} bodyText={'Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain'}/>
                            <InfoCard icon={Icon24} title={'Layanan 24 Jam'} bodyText={'Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu'}/>
                            <InfoCard icon={IconProfessional} title={'Sopir Profesional'} bodyText={'Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu'}/>
                        </div>
                    </div>
                </div>
            </div>
            <div className='row'>
                <div className='col-12' style={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', marginTop: 40}}>
                    <div style={{fontFamily: 'arial', fontSize: 24, fontWeight: 700, fontStyle: 'normal'}}>Testimonial</div>
                    <div style={{fontFamily: 'arial', fontSize: 14, fontWeight: 700, fontStyle: 'normal', marginTop: 16}}>Berbagai review positif dari para pelanggan kami</div>
                    <CarouselCard />
                </div>
            </div>
        </div>
    )
}
